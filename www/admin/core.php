<?php
/**
 * Created by JetBrains PhpStorm.
 * User: OlegReksha
 * Date: 16.01.14
 * Time: 21:00
 * To change this template use File | Settings | File Templates.
 */
class Core{
   public function Start (){
        if(isset($_GET['category'])){
            require_once('getContent.php');
            $cont = new Content();
            if($_GET['category'] == "blocks"){
                echo "edit blocks";
            }
            if($_GET['category']== 'texts'){
                if(isset($_GET['id'])){
                    $cont->getHtmlTextById($_GET['id']);
                }
                else
                $cont->getHtmlListTexts();
            }
            if($_GET['category']== 'articles'){
                if(isset($_GET['id']) && $_GET['id'] !='form'){
                    $cont->getHtmlArticleById($_GET['id']);
                }elseif($_GET['id']== 'form'){
                    $cont->getHtmlArticleById();
                }
                else
                    $cont->getHtmlListArticles();
            }
            if($_GET['category']=="contacts"){
                if(isset($_GET['id']) && $_GET['id'] !='form'){
                    $cont->getHTMLContactForm($_GET['id']);
                }elseif($_GET['id']== 'form'){
                    $cont->getHTMLContactForm();
                }
                else
                    $cont->getHtmlListContacts();
            }
            if($_GET['category']=="resumes"){
                if(isset($_GET['id'])){
                    $cont->getHTMLResumeForm($_GET['id']);
                }elseif($_GET['id']== 'form'){
                    $cont->getHTMLResumeForm();
                }
                else
                    $cont->getHtmlListResumes();
            }
            if($_GET['category']=="messages"){
                if(isset($_GET['id'])){
                    $cont->getHTMLmailform($_GET['id']);
                }
                else
                    $cont->getHTMLMessageList();
            }
        }
       else
           include("admin.html");
    }
}