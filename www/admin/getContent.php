<?php

class Content{
   function  getOnlyText($categoryId, $id ){

    }
    function  getHtmlListTexts(){
        require_once("../classes/DBase.php");
        $base = new DB();
       $res = $base->Query("select *, (select category from categories where id = categoryId) as cat from  texts");
        $html = "<table border='0' cellspacing='0'>";
        $html .="<thead><tr><td>id</td> <td>категория</td><td>дата добавления</td></tr></thead>";
        foreach($res as $k => $val){
            $html .="<tr>";
            $html .="<td>". $val['id']."</td><td>".$val['cat']."</td><td><span>". $val['date']."</span></td>";
            $html .="</tr>";
        }
        unset($base);

        $html .="</table>";
        print $html;
    }
    function  getHtmlListArticles(){
        require_once("../classes/DBase.php");
        $base = new DB();
        $res = $base->Query("select *, (select categories.category from categories where id = articles.category) as cat from  articles");
        $html = "<table border='0' cellspacing='0'>";
        $html .="<thead><tr><td>id</td> <td>категория</td><td>дата добавления</td></tr></thead>";
        foreach($res as $k => $val){
            $html .="<tr>";
            $html .="<td>". $val['id']."</td><td>".$val['cat']."</td><td><span>". $val['date']."</span></td>";
            $html .="</tr>";
        }
        unset($base);
        $html .="<tr><td colspan='3' id='add'><img src='../design/img/plus.png' alt='add' > Добавить статью</td> </tr>";
        $html .="</table>";
        print $html;
    }
    function getHtmlArticleById($id){
        require_once("../classes/DBase.php");
        $base = new DB();

        $res = $base->Query("select text from articles where id= ".$id);
        foreach($res as $k => $val)
            echo $val['text'];
        unset($base);
    }
    function getHtmlTextById($id){
        require_once("../classes/DBase.php");
        $base = new DB();

        $res = $base->Query("select text from texts where id= ".$id);
        foreach($res as $k => $val)
            echo $val['text'];
unset($base);
    }

    function getHtmlListContacts(){
        require_once("../classes/DBase.php");

        $base = new DB();
        $res = $base->Query("select *, (select categories.category from categories where id = contacts.category)as cat from contacts");
        $html = "<table border='0' cellspacing='0'>";
        $html .= "<thead><tr><td>id</td> <td>Имя</td><td>Категория</td><td></td></tr></thead>";
        foreach($res as $key =>$val){
            $html .="<tr>";
            $html .="<td>". $val['id']."</td><td>".$val['name']."</td><td>". $val['cat']."</td><td><img id='". $val['id']."' class='delbtn' src='../design/img/delete.png'></td>";
            $html .="</tr>";
        }
        $html .="<tr><td colspan='3' id='add'><img src='../design/img/plus.png' alt='add' > Добавить контакт</td> </tr>";
        $html .="</table>";
        echo $html;
unset ($base);
    }
   function  getHTMLContactForm($id =null){
       if(id != null){
       require_once("../classes/DBase.php");
       $base = new DB();
       $res = $base->Query("select * from contacts where id= ".$id);
       }
       $res1 = $base ->Query("select * from categories where type = 'contacts'");
       $select = "";
       foreach($res1 as $key => $value){
           if($value['id'] == $res[0]['categoryId'])
           $select .="<option selected=\"selected\" value='".$value['id']."'>".$value['category']."</option>";
           else
               $select .="<option value='".$value['id']."'>".$value['category']."</option>";
       }
          include("../forms/frmcontact.php");
       unset($base);
   }
    function getHTMLResumeForm($id=null){
        if(id != null){}
            require_once("../classes/DBase.php");
            $base = new DB();
            $res = $base->Query("select * from resume where id= ".$id);

        $res1 = $base ->Query("select * from categories where type = 'resumes'");
        $select = "";
        foreach($res1 as $key => $value){
            if($value['id'] == $res[0]['categoryId'])
                $select .="<option selected=\"selected\" value='".$value['id']."'>".$value['category']."</option>";
            else
                $select .="<option value='".$value['id']."'>".$value['category']."</option>";
        }
        include("../forms/exampleform.php");
        unset($base);
    }
    function formBuilder($table=null, $cat=null){
        require_once("../classes/DBase.php");
        $base = new DB();
        if($table != "categories"){
            $html="<form>";
           $res =  $base->Query("SHOW COLUMNS FROM ".$table);
            foreach($res as $k=>$v){
                if($k['Field'] == "categoryId"){
                   $html .= $this->formBuilder("categories",$cat);
                }
                if($k['Field']== "screenshot"){
                    $html .="<input type=\"file\" name=\"".$k['Field']."\" >";
                }
                $html .= "<input type=\"text\""."name=\"".$k['Field']."\">";
            }
            $html .= "</form>";
            echo $html;
        }
        else{
            $html ="<select>";
            if($cat == null)
           $res = $base ->Query("select * from categories");
            else
                $res = $base->Query("select * from categories where type = ".$cat);
            foreach($res as $key => $value){
                $html .="<option value=\"".$value['id']."\">".$value['category']."</option>";
            }
            $html .="</select>";
            return $html;
        }
    }
    function getHtmlListResumes(){
        require_once("../classes/DBase.php");

        $base = new DB();
        $res = $base->Query("select *, (select categories.category from categories where categories.id = category)as cat from resume");
        $html = "<table border='0' cellspacing='0'>";
        $html .= "<thead><tr><td>id</td> <td>Название</td><td>Ссылка</td></tr></thead>";
        foreach($res as $k=>$val){
            $html .="<tr>";
            $html .="<td>". $val['id']."</td><td>".$val['name']."</td><td>". $val['link']."</td><td><img id='". $val['id']."' class='delbtn' src='../design/img/delete.png'></td>";
            $html .="</tr>";
        }
        $html .="<tr><td colspan='3' id='add'><img src='../design/img/plus.png' alt='add' > Добавить работу</td> </tr>";
        $html .="</table>";
        echo $html;
    }
    function getHTMLMessageList(){
        require_once("../classes/DBase.php");
        $base = new DB();
        $res = $base->Query("select * from messages");
        $html = "<table border='0' cellspacing='0'>";
        $html .= "<thead><tr><td>id</td> <td>От</td><td>e-mail</td><td>Сообщение</td></tr></thead>";
        foreach($res as $k=>$val){
            $html .="<tr>";
            $html .="<td>". $val['id']."</td><td>".$val['name']." ".$val['surname']."</td><td> <a href='mailto:".$val['mail']."'>".$val['mail']."</a></td><td>". $val['message']."</td><td><img id='". $val['id']."' class='delbtn' src='../design/img/delete.png'></td>";
            $html .="</tr>";
        }
        echo $html;
    }
    function getHTMLmailform($id=null){
        require_once("../classes/DBase.php");
        $base = new DB();
        $res = $base->Query("select * from messages where id= ".$id);
        include("../forms/mailfrm.php");
        unset($base);
    }
}