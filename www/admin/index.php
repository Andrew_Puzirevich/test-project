<?php

session_start();
error_reporting( E_ERROR );
if(isset($_SESSION['auth'])){
    require_once("core.php");
    $core = new Core();
    $core->Start();
}else{
    include "login.html";
}