<!DOCTYPE html>
<html>
<head>
	<title>Best Program Solution Technolohies</title>
    <link rel="stylesheet" type="text/css" href="site/design/style.css" />
    <link rel="stylesheet" type="text/css" href="site/design/print.css" media="print" />
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<meta charset="utf-8"> 
</head>
<body>
<header>
	<div id="skyL"></div>
				<div class="logo" id="anagram">BPST</div>
		<div class="logo" id="full">Best Program Solution Technologies</div>
	<div id="skyR"></div>
	<menu>
			<ul>
				<li><a href="/">О нас</a></li>
				<li><a href="resumel">Примеры работ</a></li>
				<li><a href="contacts">Контакты</a></li>
				<li id="now"><a href="">Обратная связь</a></li>
                <li><a href="articles">Статьи</a></li>
                <li><a href="studing">Обучение</a> </li>
			</ul>
	</menu>
</header>
<div class="wraper">
<form id="form" action="javascript:void(null);" onsubmit="call()">
	<h1>Обратная связь</h1>
    <input type="hidden" value="true" name="insert">
	<input type="text" name="name" placeholder="Имя" >
	<input type="text" name="surname" placeholder="Фамилия" >
	<input type="email" name="mail" placeholder="e-mail" >
	<p><textarea placeholder="Текст..." name="message"></textarea></p>
	<p><button type="reset">Сбросить</button> <button type="submit">Отправить</button></p>
</form>
</div>
<script type="text/javascript" language="javascript">
    function call() {
        var msg   = $('#form').serialize();
        $.ajax({
            type: 'POST',
            url: '/classes/msg.php',
            data: msg,
            success: function(data) {
                alert(data);
            },
            error:  function(xhr, str){
                alert('Возникла ошибка: ' + xhr.responseCode);
            }
        });

    }
</script>
<footer>
	<div class="block">
        <?php echo $blockContacts; ?>
	</div>
	<div class="block">
		<h1>Карта</h1>
        <?php echo $blockMenu; ?>
	</div>
    <div class="block">
        <?php echo $blockSocials; ?>
    </div>
	<div id="copy">
		Сайт разработан BPST &copy; в 2014 году. Главный разработчик Олег Рекша.
	</div>
</footer>
</body>
</html>