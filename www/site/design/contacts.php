<!DOCTYPE html>
<html>
<head>
	<title>Best Program Solution Technolohies</title>
    <link rel="stylesheet" type="text/css" href="site/design/style.css" />
    <link rel="stylesheet" type="text/css" href="site/design/print.css" media="print" />
	<meta charset="utf-8"> 
</head>
<body>
<header>
	<div id="skyL"></div>
				<div class="logo" id="anagram">BPST</div>
		<div class="logo" id="full">Best Program Solution Technologies</div>
	<div id="skyR"></div>
	<menu>
			<ul>
				<li><a href="/">О нас</a></li>
				<li><a href="resume">Примеры работ</a></li>
				<li id="now"><a href="">Контакты</a></li>
				<li><a href="callback">Обратная связь</a></li>
                <li><a href="articles">Статьи</a></li>
                <li><a href="studing">Обучение</a> </li>

			</ul>
	</menu>
</header>
<div class="wraper">
	<aside class="navigation">
		<ul>
			<li><a href="#DoIt">Сделать заказ</a></li>
			<li><a href="#ContactManager">Свзятаться с менеджером</a></li>
			<li><a href="#Developers">Наши разработчики</a></li>
		</ul>
	</aside>
<div class="contacts">
	<a name="DoIt"></a>
<h1>Сделать заказ</h1>
		<div class="contact">
			Для того чтобы заказать сайт обратитесь к нам через:
			<div class="clear"></div>
			<ul>
				<li>Skype: metatron60</li>
				<li>e-mail: masterjada@gmail.com</li>
				<li>тел. (093)706-08-62 Олег</li>
			</ul>
			<div id="site"><img src="site/design/src/img/site.png" alt="web" /></div>
			<div class="clear"></div>
			</div>

			<div class="contact">
			Для того чтобы заказать мобильное приложение Android, Windows Phone:
			<div class="clear"></div>
			<ul>
				<li>Skype: metatron60</li>
				<li>e-mail: masterjada@gmail.com</li>
				<li>тел. (093)706-08-62 Олег</li>
			</ul>
			<div id="mobile"><img src="site/design/src/img/phone.png" alt="phone" /></div>
			<div class="clear"></div>
			</div>
			<div class="contact">
			Для того чтобы заказать desctop:
			<div class="clear"></div>
			<ul>
				<li>Skype: metatron60</li>
				<li>e-mail: masterjada@gmail.com</li>
				<li>тел. (093)706-08-62 Олег</li>
			</ul>
			<div id="desctop"><img src="site/design/src/img/comp.png" alt="comp" /></div>
			<div class="clear"></div>
			</div>

			<a name="ContactManager"></a>
<h1>Связь с менеджером</h1>
		<div class="contact">
			В нашей компании менеджер это человек который отвечает за то чтобы человек был доволен выполненой работой.
			По этому в случае если вас не устраивает качество работы, вы хотите внести какое-то предложение (например партнерская программа) или просто хотите нас поблагодарить. Свяжитесь с нашим менджером.
			<div class="clear"></div>
			<ul>
				<li>Skype: metatron60</li>
				<li>e-mail: masterjada@gmail.com</li>
				<li>тел. (093)706-08-62 Олег</li>
			</ul>
			<div id="manager"><img src="site/design/src/img/manager.png" alt="manager" /></div>
			<div class="clear"></div>
			</div>
			<section id="developers">
				<a name="Developers"></a>
				<h1>Разработчики</h1>
                <?php if($contacts){?>
                    <?php foreach($contacts as $key =>$value){ ?>
	<div class="dev">
		<div class="pic">
			<a href="#"><img src="<?php echo $value['photo']; ?>" alt="avatar"/></a>
		</div> 
			<p><?php echo $value['name'] ?></p>
			<p><?php echo $value['description']; ?></p>
		</div>
                <?php }} ?>
	
</section>
</div>
<div class="clear"></div>

</div>
<footer>
    <div class="block">
        <?php echo $blockContacts; ?>
    </div>
    <div class="block">
        <h1>Карта</h1>
        <?php echo $blockMenu; ?>
    </div>
    <div class="block">
        <?php echo $blockSocials; ?>
    </div>
    <div id="copy">
        Сайт разработан BPST &copy; в 2014 году. Главный разработчик Олег Рекша.
    </div>
</footer>
</body>
</html>