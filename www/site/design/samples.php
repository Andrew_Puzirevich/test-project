<!DOCTYPE html>
<html>
<head>
	<title>Best Program Solution Technolohies</title>
    <link rel="stylesheet" type="text/css" href="site/design/style.css" />
    <link rel="stylesheet" type="text/css" href="site/design/print.css" media="print" />
	<meta charset="utf-8"> 
</head>
<body>
<header>
	<div id="skyL"></div>
				<div class="logo" id="anagram">BPST</div>
		<div class="logo" id="full">Best Program Solution Technologies</div>
	<div id="skyR"></div>
	<menu>
			<ul>
				<li><a href="/">О нас</a></li>
				<li id="now"><a href="">Примеры работ</a></li>
				<li><a href="contacts">Контакты</a></li>
				<li><a href="callback">Обратная связь</a></li>
                <li><a href="articles">Статьи</a></li>
                <li><a href="studing">Обучение</a> </li>
			</ul>
	</menu>
</header>
<div class="wraper">
	<aside class="navigation">
		<ul>
            <?php if($web){ ?>
			<li><a href="#web">Веб-сайты</a></li>
            <?php } if($mobile){ ?>
			<li><a href="#mobile">Мобильные приложения</a></li>
            <?php } if($desctop){ ?>
			<li><a href="#desctop">Приложения desctop</a></li>
            <?php } ?>
		</ul>
	</aside>
	
		<div id="samples">
            <?php if($web){ ?>
			<a name="web"></a>
			<div id="header">Веб сайты</div>
				<ul>
                    <?php foreach($web as $key=>$value){ ?>
	<li><a href="<?php echo "/sample/".$value['id']; ?>"><img src="<?php echo $value['screenshot']; ?>" alt="<?php echo $value['name']; ?>" /></a><p><?php echo $value['name']; ?></p></li>
				<?php }?>
				</ul>
            <?php } if($mobile){ ?>
				<a name="mobile"></a>
				<div id="header">Мобильные приложения</div>
				<ul>
                    <?php foreach($mobile as $key=>$value){ ?>
                        <li><a href="<?php echo "/sample/".$value['id']; ?>"><img src="<?php echo $value['screenshot']; ?>" alt="<?php echo $value['name']; ?>" /><p><?php echo $value['name']; ?></p></a></li>
                    <?php }?>

				</ul><?php } if($desctop){ ?>
				<a name="desctop"></a>
				<div id="header">Приложения desctop</div>
				<ul>
                    <?php foreach($desctop as $key=>$value){ ?>
                        <li><a href="<?php echo "/sample/".$value['id']; ?>"><img src="<?php echo $value['screenshot']; ?>" alt="<?php echo $value['name']; ?>" /></p></a><p><?php echo $value['name']; ?></li>
                    <?php }}?>
				</ul>
		</div>
		<div class="clear"></div>
	</div>
<footer>
	<div class="block">
        <?php echo $blockContacts; ?>
	</div>
	<div class="block">
		<h1>Карта</h1>
        <?php echo $blockMenu; ?>
	</div>
    <div class="block">
        <?php echo $blockSocials; ?>
    </div>
	<div id="copy">
		Сайт разработан BPST &copy; в 2014 году. Главный разработчик Олег Рекша.
	</div>
</footer>
</body>
</html>