<!DOCTYPE html>
<html>
<head>
	<title>Best Program Solution Technolohies</title>
	<link rel="stylesheet" type="text/css" href="site/design/style.css" />
	<link rel="stylesheet" type="text/css" href="site/design/print.css" media="print" />
	<meta charset="utf-8"> 
</head>
<body>
<header>
	<div id="skyL"></div>
				<div class="logo" id="anagram">BPST</div>
		<div class="logo" id="full">Best Program Solution Technologies</div>
	<div id="skyR"></div>
	<menu>
			<ul>
				<li ><a href="">О нас</a></li>
				<li><a href="resume">Примеры работ</a></li>
				<li><a href="contacts">Контакты</a></li>
				<li><a href="callback">Обратная связь</a></li>
                <li><a href="articles">Статьи</a></li>
                <li id="now"><a href="studing">Обучение</a> </li>
			</ul>
	</menu>
</header>
<div class="wraper">
	<div id="content">

<?php echo $text; ?>
</div></div>
<footer>
	<div class="block">
		<?php echo $blockContacts; ?>
	</div>
	<div class="block">
		<h1>Карта</h1>
		<?php echo $map; ?>
	</div>
    <div class="block">
        <?php echo $blockSocials; ?>
    </div>
	<div id="copy">
		Сайт разработан BPST &copy; в 2014 году. Главный разработчик Олег Рекша.
	</div>
</footer>
</body>
</html>